#include "configuration.hpp"

#include "config.hpp"

Elis::Configuration::Configuration() {}
Elis::Configuration::Configuration(std::string f) : filename(f) {}

Elis::Configuration*
Elis::Configuration::parse(int argc, char const *argv[])
{
    std::string filename;

    if (argc > 1)
        filename = argv[1];
    else
        filename = Elis::Config::DEFAULT_FILENAME;

    return new Elis::Configuration(filename);
}

std::string
Elis::Configuration::get_filename()
{
    return this->filename;
}
