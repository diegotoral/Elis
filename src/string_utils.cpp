#include "string_utils.hpp"

Elis::List<std::string>*
StringUtils::split(std::string str, const char c)
{
    std::string::size_type i = 0;
    std::string::size_type j = str.find(c);
    auto list = new Elis::List<std::string>;

    while (j != std::string::npos)
    {
        list->push_back(str.substr(i, j-i));
        i = ++j;
        j = str.find(c, j);

        if (j == std::string::npos)
            list->push_back(str.substr(i, str.length()));
    }

    return list;
}
