#include "modes/insert.hpp"

#include <ncurses.h>

#include "actions.hpp"
#include "config.hpp"
#include "widgets/file_view.hpp"

Elis::Modes::Insert::Insert(Elis::Application* app)
{
    this->window = new Elis::Widgets::FileView(app->get_file());
}

Elis::Modes::Insert::~Insert()
{
    delete this->window;
}

int
Elis::Modes::Insert::run()
{
    int key;

    this->window->show();
    this->window->refresh();

    switch ((key = wgetch(this->window->get_window())))
    {
        case Elis::Config::Keys::ESCAPE:
            return Elis::ACTION_TYPE::TOGGLE_NORMAL_MODE;
    }

    return Elis::ACTION_TYPE::DO_NOTHING;
}

std::string
Elis::Modes::Insert::get_input()
{
    return "";
}
