#include "modes/normal.hpp"

#include <ncurses.h>
#include <sstream>

#include "actions.hpp"
#include "config.hpp"
#include "widgets/command_window.hpp"

Elis::Modes::Normal::Normal(Elis::Application* app) : command("")
{
    this->window = new Elis::Widgets::CommandWindow(&command);
}

Elis::Modes::Normal::~Normal()
{
    delete this->window;
}

int
Elis::Modes::Normal::run()
{
    int key;

    this->window->show();
    this->window->refresh();

    switch ((key = wgetch(this->window->get_window())))
    {
        case Elis::Config::Keys::ESCAPE:
            this->window->hide();
            return Elis::ACTION_TYPE::TOGGLE_INSERT_MODE;
        case Elis::Config::Keys::ENTER:
            this->window->hide();
            return this->get_action_from_command();
        case KEY_DC:
        case KEY_BACKSPACE:
            this->command.pop_back();
        default:
            if (key >= 32 && key <= 127)
                this->command.push_back((char) key);
    }

    return Elis::ACTION_TYPE::DO_NOTHING;
}

std::string
Elis::Modes::Normal::get_input()
{
    return this->command;
}

// private

int
Elis::Modes::Normal::get_action_from_command()
{
    switch (this->command.front())
    {
        case 'q':
        case 'Q':
            return Elis::ACTION_TYPE::EXIT;
        case 'w':
        case 'W':
            return Elis::ACTION_TYPE::SAVE;
        case 'h':
        case 'H':
            return Elis::ACTION_TYPE::HELP;
        case 'e':
        case 'E':
            return Elis::ACTION_TYPE::OPEN;
    }

    return Elis::ACTION_TYPE::TOGGLE_INSERT_MODE;
}
