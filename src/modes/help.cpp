#include "modes/help.hpp"

#include "config.hpp"
#include "actions.hpp"
#include "widgets/help_window.hpp"

Elis::Modes::Help::Help(Elis::Application* app)
{
    this->window = new Elis::Widgets::HelpWindow();
}

Elis::Modes::Help::~Help()
{
    delete this->window;
}

int
Elis::Modes::Help::run()
{
    this->window->show();
    this->window->refresh();

    switch (wgetch(this->window->get_window()))
    {
        case Elis::Config::Keys::ESCAPE:
            this->window->hide();
            return Elis::ACTION_TYPE::TOGGLE_INSERT_MODE;
    }

    return Elis::ACTION_TYPE::DO_NOTHING;
}

std::string
Elis::Modes::Help::get_input()
{
    return "";
}
