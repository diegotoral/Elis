#include "line.hpp"


Elis::Line::Line() : content("") {}
Elis::Line::Line(std::string c) : content(c) {}


void
Elis::Line::set_content(std::string c)
{
    this->content = c;
}

const std::string
Elis::Line::get_content()
{
    return this->content;
}


const bool
Elis::Line::is_current()
{
    return this->current;
}

void
Elis::Line::set_current(bool c = false)
{
    this->current = c;
}
