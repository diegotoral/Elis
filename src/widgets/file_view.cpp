#include "widgets/file_view.hpp"

Elis::Widgets::FileView::FileView(Elis::File* f) : file(f)
{
    this->lines = new Elis::List<Elis::Widgets::LineView>();
}

Elis::Widgets::FileView::~FileView()
{
    delete this->lines;
}

void
Elis::Widgets::FileView::show()
{
    this->window = newwin(LINES, COLS, 0, 0);
    this->wrap_lines();

    auto lines = this->lines;
    // auto size = lines->size();

    for (auto it = lines->begin(); it != lines->end(); it++)
        (*it).show();

    wrefresh(this->window);
}

void
Elis::Widgets::FileView::hide()
{
    wclear(this->window);
    delwin(this->window);
}

void
Elis::Widgets::FileView::refresh()
{
    wclear(this->window);

    for (auto it = lines->begin(); it != lines->end(); it++)
        (*it).refresh();

    wrefresh(this->window);
}


// private
// =======

void
Elis::Widgets::FileView::wrap_lines()
{
    auto index = 1;
    auto lines = this->file->get_lines();

    this->lines->clear();

    for (auto it = lines->begin(); it != lines->end(); it++)
    {
        auto view = Elis::Widgets::LineView(this->window, index++, &(*it));
        this->lines->push_back(view);
    }

}
