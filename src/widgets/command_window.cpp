#include "widgets/command_window.hpp"

#include <ncurses.h>

Elis::Widgets::CommandWindow::CommandWindow(std::string* c) : command(c) {}

void
Elis::Widgets::CommandWindow::show()
{
    this->window = newwin(1, COLS, LINES - 1, 0);
    keypad(this->window, true);
    this->draw();
    // wrefresh(this->window);
}

void
Elis::Widgets::CommandWindow::hide()
{
    wclear(this->window);
    wrefresh(this->window);
    delwin(this->window);
}

void
Elis::Widgets::CommandWindow::refresh()
{
    this->hide();
    this->show();
    wrefresh(this->window);
}

WINDOW*
Elis::Widgets::CommandWindow::get_window()
{
    return this->window;
}


// private
// =======

void
Elis::Widgets::CommandWindow::draw()
{
    wprintw(this->window, ": %s", this->command->c_str());
    wmove(this->window, LINES - 1, this->command->size() + 2);
}
