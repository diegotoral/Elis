#include "widgets/line_view.hpp"

#include <ncurses.h>
#include "config.hpp"

Elis::Widgets::LineView::LineView() : index(1), line(new Elis::Line)
{}

Elis::Widgets::LineView::LineView(WINDOW* w, int i, Elis::Line* l) : index(i), line(l)
{
    this->window = w;
}

Elis::Widgets::LineView::~LineView()
{}

void
Elis::Widgets::LineView::show()
{
    auto content = this->line->get_content();

    wprintw(this->window, this->get_format(), this->index, content.c_str());
    // wmove(this->window, index, content.size());
}


void
Elis::Widgets::LineView::hide()
{
    // wclrtoeol()
}

void
Elis::Widgets::LineView::refresh()
{
    this->hide();
    this->show();
}

// Private
// =======

const char * const
Elis::Widgets::LineView::get_format()
{
    auto current = this->line->is_current();
    return current ? Elis::Config::CURRENT_LINE_FORMAT : Elis::Config::LINE_FORMAT;
}
