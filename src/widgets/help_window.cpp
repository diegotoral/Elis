#include "widgets/help_window.hpp"

Elis::Widgets::HelpWindow::HelpWindow() {}

Elis::Widgets::HelpWindow::~HelpWindow()
{}

void
Elis::Widgets::HelpWindow::show()
{
    this->window = newwin(LINES, COLS, 0, 0);

    wprintw(this->window, "Ajuda do Elis (pressione ESC para voltar ao modo de edição)\n");
    whline(this->window, '-', COLS);
    wmove(this->window, 3, 0);

    wprintw(this->window, "W [<name>]      Salva todas as linhas do texto em um arquivo ascii name.\n");
    wprintw(this->window, "E <name>        Lê para a memória todas as linhas de texto do arquivo ascii name.\n");
    wprintw(this->window, "I [n]           Entra no modo de edição, permitindo a inserção de texto antes da linha n.\n");
    wprintw(this->window, "A [n]           Entra no modo de edição, permitindo a inserção de texto depois da linha n.\n");
    wprintw(this->window, "M [n]           Torna n a linha atual.\n");
    wprintw(this->window, "D [n [m]]       Remove linhas n até m.\n");
    wprintw(this->window, "L [n [m]]       Lista as linhas n até m.\n");
    wprintw(this->window, "H               Exibe este texto de ajuda.\n");
    wprintw(this->window, "Q               Encerra o programa.");

    wrefresh(this->window);
}

void
Elis::Widgets::HelpWindow::hide()
{
    delwin(this->window);
}

void
Elis::Widgets::HelpWindow::refresh()
{
    this->hide();
    this->show();
    wrefresh(this->window);
}
