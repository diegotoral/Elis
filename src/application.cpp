#include "application.hpp"

#include <iostream>
#include "easylogging++.h"


#include "actions.hpp"
#include "config.hpp"
#include "exception.hpp"
#include "modes/help.hpp"
#include "modes/insert.hpp"
#include "modes/normal.hpp"
#include "widgets/base.hpp"


Elis::Application* Elis::Application::singleton_instance = 0;

Elis::Application::Application() : exiting(false)
{}

Elis::Application::~Application()
{
    LOG(INFO) << "Encerrando a aplicação.";

    delete this->file;
    delete this->config;
}

Elis::Application&
Elis::Application::initialize(int argc, char const *argv[])
{
    auto app = Elis::Application::instance();

    app->config = Elis::Configuration::parse(argc, argv);
    app->file = Elis::File::from_config(app->config);
    app->mode = new Elis::Modes::Insert(app);

    return *app;
}

Elis::Application*
Elis::Application::instance()
{
    if (!singleton_instance)
        singleton_instance = new Elis::Application();

    return singleton_instance;
}

void
Elis::Application::run()
{
    LOG(INFO) << "Executando laço principal da aplicação.";

    Elis::Widgets::Base::initialize();

    while (this->is_running())
    {
        switch (this->mode->run())
        {
            case Elis::ACTION_TYPE::HELP:
                this->toggle_help_mode();
                break;
            case Elis::ACTION_TYPE::TOGGLE_NORMAL_MODE:
                this->toggle_normal_mode();
                break;
            case Elis::ACTION_TYPE::TOGGLE_INSERT_MODE:
                this->toggle_insert_mode();
                break;
            case Elis::ACTION_TYPE::EXIT:
                this->set_exiting();
                break;
            case Elis::ACTION_TYPE::SAVE:
                this->save();
                this->toggle_insert_mode();
                break;
            case Elis::ACTION_TYPE::OPEN:
                this->open();
                this->toggle_insert_mode();
                break;
        }
    }

    Elis::Widgets::Base::finalize();
}

const bool
Elis::Application::is_running()
{
    return !this->exiting;
}

void
Elis::Application::set_exiting()
{
    LOG(INFO) << "Aplicação configurada para sair.";
    this->exiting = true;
}

void
Elis::Application::save()
{
    auto params = this->mode->get_params();

    this->file->save(params->back());
}


void
Elis::Application::open()
{
    auto params = this->mode->get_params();

    this->file = Elis::File::from_path(params->back());
}

Elis::File*
Elis::Application::get_file()
{
    return this->file;
}

void
Elis::Application::toggle_insert_mode()
{
    LOG(INFO) << "Alternando estado para: insert";
    this->mode = new Elis::Modes::Insert(this);
}

void
Elis::Application::toggle_normal_mode()
{
    LOG(INFO) << "Alternando estado para: normal";
    this->mode = new Elis::Modes::Normal(this);
}

void
Elis::Application::toggle_help_mode()
{
    LOG(INFO) << "Alternando estado para: help";
    this->mode = new Elis::Modes::Help(this);
}
