#include <iostream>
#include <ncurses.h>

#define DELPP_DEFAULT_LOG_FILE = '"logs/elis.log"';

#include "easylogging++.h"
#include "application.hpp"

INITIALIZE_EASYLOGGINGPP

int main(int argc, char const *argv[])
{
    START_EASYLOGGINGPP(argc, argv);

    LOG(INFO) << "Inicializando Elis.";

    try
    {
        auto app = Elis::Application::initialize(argc, argv);
        app.run();
    }
    catch(std::exception& e)
    {
        LOG(ERROR) << "Oops, something went wrong! :(";

        exit (EXIT_FAILURE);
    }

    return 0;
}
