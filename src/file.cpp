#include "file.hpp"

#include <fstream>
#include "easylogging++.h"

#include "exception.hpp"

Elis::File::File(Elis::Configuration* c)
    : config(c), current_line(nullptr), lines(new Elis::List<Elis::Line>) {}

Elis::File::~File()
{
    delete this->lines;
    delete current_line;
}

Elis::File*
Elis::File::from_config(Elis::Configuration* config)
{
    auto file = new Elis::File(config);
    std::ifstream stream(config->get_filename());

    LOG(INFO) << "Carregando arquivo do disco.";

    if (stream.fail())
        throw Elis::Exception(std::string("Can't open file: ") + config->get_filename());

    file->load_lines(&stream);

    return file;
}

Elis::File*
Elis::File::from_path(std::string path)
{
    auto config = new Configuration(path);
    auto file = Elis::File::from_config(config);

    LOG(INFO) << "Carregando arquivo de caminho: " << path;

    return file;
}

int
Elis::File::size()
{
    return this->lines->size();
}

Elis::List<Elis::Line>*
Elis::File::get_lines()
{
    return this->lines;
}

Elis::Line*
Elis::File::get_current_line()
{
    return this->current_line;
}

void
Elis::File::set_current_line(int index)
{
    int i = 1;

    for (auto it = this->lines->begin(); it != this->lines->end(); it++)
    {
        if (i == index)
            *(this->current_line) = *it;

        i++;
    }
}

void
Elis::File::remove_line(int number)
{
    auto line_it = this->lines->begin();

    for (int i = 1;number < i; i++)
        ++line_it;

    this->lines->erase(line_it);
}

void
Elis::File::remove_lines(int start, int end)
{
    auto start_it = this->lines->begin();
    auto end_it = this->lines->begin();

    for (int i = 1;start < i; i++)
        ++start_it;

    for (int i = 1;end > i; i++)
        ++end_it;

    this->lines->erase(start_it, end_it);
}

const std::string
Elis::File::get_content()
{
    std::string content;

    for (auto it = this->lines->begin(); it != this->lines->end(); it++)
        content += (*it).get_content();

    return content;
}

void
Elis::File::save(std::string filename)
{
    std::ofstream stream(filename, std::ios::trunc);

    LOG(INFO) << "Salvando arquivo: " << filename;

    if (stream.fail())
        throw Elis::Exception(std::string("Can't save file: ") + filename);

    for (auto it = this->lines->begin(); it != this->lines->end(); ++it)
        stream << (*it).get_content() << '\n';
}

// private
// =======

void
Elis::File::load_lines(std::ifstream* stream)
{
    std::string content;

    while (std::getline(*stream, content))
    {
        this->lines->push_back(Elis::Line(content));
    }

    // Configura a última linha como a atual, por padrão.
    this->current_line = &this->lines->back();
    this->current_line->set_current(true);
}
