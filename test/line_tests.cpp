#include "catch.hpp"

#include "line.hpp"

SCENARIO("we can read line's content")
{
    GIVEN("a line")
    {
        auto line = Elis::Line("foobar");

        THEN("we can get the line's content")
        {
            REQUIRE(line.get_content() == "foobar");
        }
    }
}

SCENARIO("we can change line's content entirely")
{
    GIVEN("a line")
    {
        auto line = Elis::Line("foobar");

        REQUIRE(line.get_content() == "foobar");

        WHEN("we set the line's content")
        {
            line.set_content("barfoo");

            THEN("we can get the new content")
            {
                REQUIRE(line.get_content() == "barfoo");
            }
        }
    }
}
