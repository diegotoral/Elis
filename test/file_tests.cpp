#include "catch.hpp"

#include "file.hpp"
#include "configuration.hpp"

SCENARIO("we can load a file from a configuration object", "[file]")
{
    GIVEN("a configuration for an existing file")
    {
        auto config = new Elis::Configuration("test/fixtures/foobar.txt");

        REQUIRE(config->get_filename() == "test/fixtures/foobar.txt");

        WHEN("we load from configuration")
        {
            auto file = Elis::File::from_config(config);

            THEN("we can get the file content")
            {
                REQUIRE(file->get_content() == "Hello, from the file.");
            }
        }
    }
}

SCENARIO("we can get file size", "[file]")
{
    GIVEN("a configuration for a file with some lines")
    {
        auto config = new Elis::Configuration("test/fixtures/fruits.txt");

        WHEN("we load from configuration")
        {
            auto file = Elis::File::from_config(config);

            THEN("file has size equals the file lines number")
            {
                REQUIRE(file->size() == 5);
            }
        }
    }
}

SCENARIO("we can get the current line", "[file]")
{
    GIVEN("an empty file")
    {
        auto file = new Elis::File();

        REQUIRE(file->size() == 0);

        THEN("the current line is null")
        {
            REQUIRE(file->get_current_line() == nullptr);
        }
    }

    GIVEN("a non empty file")
    {
        auto config = new Elis::Configuration("test/fixtures/fruits.txt");
        auto file = Elis::File::from_config(config);

        WHEN("we get the current line")
        {
            THEN("it isn't null")
            {
                REQUIRE(file->get_current_line() != nullptr);
            }

            THEN("it defaults to the last line in the file")
            {
                auto content = file->get_current_line()->get_content();

                REQUIRE(content == "tomato");
            }
        }
    }
}

SCENARIO("we can set the current line")
{
    GIVEN("an empty file")
    {
        auto file = new Elis::File();

        REQUIRE(file->get_current_line() == nullptr);

        WHEN("set the current line to any value")
        {
            file->set_current_line(1);

            THEN("it returns null")
            {
                REQUIRE(file->get_current_line() == nullptr);
            }
        }
    }

    GIVEN("a non empty file")
    {
        auto config = new Elis::Configuration("test/fixtures/fruits.txt");
        auto file = Elis::File::from_config(config);

        auto content = file->get_current_line()->get_content();
        REQUIRE(content == "tomato");

        WHEN("set current line to invalid number")
        {
            file->set_current_line(0);

            THEN("the current line don't change")
            {
                auto content = file->get_current_line()->get_content();

                REQUIRE(content == "tomato");
            }
        }

        WHEN("set to a valid line")
        {
            file->set_current_line(1);

            THEN("the current line is set correctly")
            {
                auto content = file->get_current_line()->get_content();

                REQUIRE(content == "apple");
            }
        }
    }
}

SCENARIO("we can remove a single line", "[list]")
{
    GIVEN("a non empty file")
    {
        auto config = new Elis::Configuration("test/fixtures/fruits.txt");
        auto file = Elis::File::from_config(config);

        REQUIRE(file->size() == 5);

        WHEN("the first line is removed")
        {
            file->remove_line(1);

            THEN("the size changes")
            {
                REQUIRE(file->size() == 4);
            }

            THEN("the content changes")
            {
                auto content = file->get_content();

                REQUIRE(content == "orangebananalemontomato");
            }
        }
    }
}

SCENARIO("we can remove lines", "[file]")
{
    GIVEN("a non empty file")
    {
        auto config = new Elis::Configuration("test/fixtures/fruits.txt");
        auto file = Elis::File::from_config(config);

        REQUIRE(file->size() == 5);

        WHEN("3 lines are removed")
        {
            file->remove_lines(1, 3);

            THEN("the size changes")
            {
                REQUIRE(file->size() == 3);
                REQUIRE(file->get_content() == "bananalemontomato");
            }
        }
    }
}
