#include "catch.hpp"

#include "config.hpp"
#include "configuration.hpp"

using namespace Elis;

SCENARIO("we can create a configuration manually")
{
    GIVEN("a filename")
    {
        auto filename = "foobar.txt";

        WHEN("we manually create a configuration")
        {
            auto config = Elis::Configuration(filename);

            THEN("we can get the filename form it")
            {
                REQUIRE(config.get_filename() == filename);
            }
        }
    }
}

SCENARIO("we can parse command line arguments")
{
    GIVEN("no arguments")
    {
        int argc = 0;
        char const *argv[] = { "bin/elis" };

        WHEN("parse is called")
        {
            auto config = Configuration::parse(argc, argv);

            THEN("filename returns default value")
            {
                REQUIRE(config->get_filename() == Elis::Config::DEFAULT_FILENAME);
            }
        }
    }
    GIVEN("some arguments")
    {
        int argc = 3;
        char const *argv[] = { "bin/elis", "foo", "bar" };

        WHEN("parse is called")
        {
            auto config = Configuration::parse(argc, argv);

            THEN("we can read values from an object")
            {
                REQUIRE(config->get_filename() == "foo");
            }

        }

    }
}
