ELIS
====
*elis* (Editor de Linhas Simples) é um editor de textos que armazena o texto em uma lista encadeada, onde cada nó representa uma linha. *elis* é capaz de realizar tarefas simples, como abrir e salvar arquivos, desfazer comandos e movimentar o cursor nas várias linhas do texto.

*elis* foi desenvolvido como projeto para a disciplina de *Estruturas de dados básicas I* (UFRN 2016.1).

Como compilar
-------------
### Dependências
*elis* requer quer os seguintes programas e bibliotécas estejam instalados para a devida compilação do seu executável:

* g++ ou clang
* make
* ncurses

### Compilando
Execute o comando `make` para compilar o código-fonte do projeto em um arquivo executável chamado `elis`.

    $ make

Como usar
---------
Após compilado um arquivo executável `elis` estará disponível no diretório rais do projeto, execute-o conforme mostrado abaixo:

		$ ./elis
		1*>

Limitações
----------
Abaixo estão listadas todas as limitações do programa.
