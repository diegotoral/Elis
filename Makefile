# General configuration
# =====================
CC := g++
SRCDIR := src
BUILDDIR := build
TARGET := bin/elis

# Source configuration
# ====================
SRCEXT := cpp
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
CFLAGS := -g --std=c++11 -Wall -stdlib=libstdc++
LIB := -L lib
INC := -I include

$(TARGET): $(OBJECTS)
	@echo " Linking $(TARGET)..."
	@echo " $(CC) $^ -o $(TARGET) $(LIB) -lncurses"; $(CC) $^ -o $(TARGET) $(LIB) -lncurses

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	@echo " $(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<


# Tests configuration
# ===================
TESTDIR := test
TESTTARGET := bin/tester
TESTSRCEXT := .$(SRCEXT)

TESTSOURCES := $(shell find $(TESTDIR) -type f -name *$(TESTSRCEXT))
TESTOBJECTS := $(patsubst $(TESTDIR)/%,$(BUILDDIR)/%,$(TESTSOURCES:.$(SRCEXT)=.o))
TESTLIB := -I lib -I include

tester: $(OBJECTS) $(TESTOBJECTS)
	@echo " Linking $(TESTTARGET)..."
	@echo " $(CC) $(CFLAGS) $^ $(TESTLIB) -o $(TESTTARGET)"; $(CC) $(CFLAGS) $^ $(TESTLIB) -o $(TESTTARGET)

$(BUILDDIR)/%.o: $(TESTDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	@echo " $(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(TESTLIB) -c -o $@ $<


clean:
	@echo " Cleaning...";
	@echo " $(RM) -r $(BUILDDIR) $(TARGET)"; $(RM) -r $(BUILDDIR) $(TARGET)

.PHONY: clean
