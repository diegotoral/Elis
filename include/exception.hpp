#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_

#include <string>

namespace Elis { class Exception; }

class Elis::Exception : public std::exception
{
    public:
        Exception(const char* m)
         : message(m) {}

         Exception(std::string m)
          : message(m.c_str()) {}

        ~Exception() {}

        virtual const char* what() const throw()
        {
            return message;
        }

    private:
        const char* message;
};

#endif
