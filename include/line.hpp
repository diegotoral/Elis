#ifndef _LINE_H_
#define _LINE_H_

#include <string>

namespace Elis { class Line; };

/**
 * Uma linha do arquivo de texto.
 *
 * Elis::Line encapsula o conteúdo e operações que podem ser realizadas sobre
 * uma linha de texto, aqui representada por uma std::string.
 */
class Elis::Line {
    public:
        /**
         * Inicializa uma linha vazia.
         */
        Line();

        /**
         * Inicializa uma linha com o conteúdo da string.
         *
         * @param c string com o conteúdo da linha.
         */
        Line(std::string c);

        /**
         * Sobrescreve o conteúdo da linha.
         *
         * @param c stirng com o novo conteúdo da linha.
         */
        void set_content(std::string c);

        /**
         * Retorna o conteúdo da linha integralmente.
         *
         * @return string como conteúdo da linha.
         */
        const std::string get_content();

        /*
         * Indica se está linha é a que está sendo atualmente editada.
         *
         * @return bool verdadeiro se esta linha é a atual.
         */
        const bool is_current();

        /*
         * Seta esta linha como a que está sendo atualmente editada.
         */
        void set_current(bool c);

    private:
        bool current;
        std::string content;
};

#endif
