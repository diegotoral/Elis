#ifndef _INSERT_H
#define _INSERT_H

#include "modes/base.hpp"
#include "application.hpp"

namespace Elis {
    namespace Modes { class Insert; };
};

class Elis::Modes::Insert : public Elis::Modes::Base {
    public:
        Insert(Elis::Application* app);
        ~Insert();

        int run();
        std::string get_input();
};

#endif
