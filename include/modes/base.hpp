#ifndef _BASE_H_
#define _BASE_H_

#include <string>

#include "widgets/base.hpp"
#include "string_utils.hpp"


namespace Elis {
    namespace Modes { class Base; };
};

/**
 * Interface base para os estados da aplicação.
 *
 * Cada estado da aplicação deve, obritatóriamente, implementar esta interface e
 * definir o comportamento conforme desejado.
 */
class Elis::Modes::Base {
    public:
        virtual ~Base() {
            delete this->window;
        };

        virtual int run() = 0;

        /**
         * Retorna o conteúdo digitado pelo usuário.
         *
         * @return string
         */
        virtual std::string get_input() = 0;

        /**
         * Retorna uma lista com os valores digitados pelo usuário. Inclui o
         * comando.
         *
         * Exemplo:
         *      ["E", "foobar.txt"]
         *
         * @return Elis::List<std::string>*
         */
        Elis::List<std::string>* get_params()
        {
            auto input = this->get_input();
            auto params = StringUtils::split(input, ' ');

            return params;
        }

    protected:
        Elis::Widgets::Base* window;
};

#endif
