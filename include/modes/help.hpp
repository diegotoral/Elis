#ifndef _HELP_H_
#define _HELP_H_

#include "modes/base.hpp"
#include "application.hpp"

namespace Elis {
    namespace Modes { class Help; };
};

class Elis::Modes::Help : public Elis::Modes::Base {
    public:
        Help(Elis::Application* app);
        ~Help();

        int run();
        std::string get_input();
};

#endif
