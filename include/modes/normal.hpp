#ifndef _COMMAN_H_
#define _COMMAN_H_

#include <string>
#include "modes/base.hpp"
#include "application.hpp"

namespace Elis {
    namespace Modes { class Normal; };
};

class Elis::Modes::Normal : public Elis::Modes::Base {
    public:
        Normal(Elis::Application* app);
        ~Normal();

        int run();
        std::string get_input();

    private:
        std::string command;

        int get_action_from_command();
};

#endif
