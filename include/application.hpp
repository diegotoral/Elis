#ifndef _APPLICATION_H_
#define _APPLICATION_H_

#include <string>

#include "list.hpp"
#include "file.hpp"
#include "modes/base.hpp"
#include "configuration.hpp"

namespace Elis { class Application; };

/**
 * @brief Uma instância da aplicação em memória.
 */
class Elis::Application {
    public:
        ~Application();

        /**
         * Rotina de inicialização da aplicação.
         *
         * Toda a lógica necessária para inicialização de um Elis::Application
         * está contida neste método. Dentre outras coisas, aqui é feito o
         * carregamento das configurações e arquivo de entrada.
         */
        static Application& initialize(int argc, char const *argv[]);

        static Elis::Application* instance();

        void run();

        void set_exiting();
        const bool is_running();

        void save();
        void open();

        Elis::File* get_file();

        void toggle_insert_mode();
        void toggle_normal_mode();
        void toggle_help_mode();

    private:
        bool exiting;
        Elis::File* file;
        Elis::Modes::Base* mode;
        Elis::Configuration* config;

        static Elis::Application* singleton_instance;
        Application();
};

#endif
