#ifndef _COMMAND_WINDOW_H_
#define _COMMAND_WINDOW_H_

#include <string>
#include "widgets/base.hpp"

namespace Elis {
    namespace Widgets { class CommandWindow; };
};

class Elis::Widgets::CommandWindow : public Elis::Widgets::Base {
    public:
        CommandWindow(std::string* c);

        void show();
        void hide();
        void refresh();

        WINDOW* get_window();

    private:
        std::string* command;

        void draw();
};

#endif
