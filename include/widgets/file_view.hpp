#ifndef _FILE_VIEW_H_
#define _FILE_VIEW_H_

#include <ncurses.h>

#include "file.hpp"
#include "list.hpp"
#include "widgets/base.hpp"
#include "widgets/line_view.hpp"

namespace Elis {
    namespace Widgets { class FileView; };
};

class Elis::Widgets::FileView : public Elis::Widgets::Base {
    public:
        FileView(Elis::File* f);
        ~FileView();

        void show();
        void hide();
        void refresh();

    private:
        Elis::File* file;
        Elis::List<Elis::Widgets::LineView>* lines;

        void wrap_lines();
};

#endif
