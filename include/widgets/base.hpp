#ifndef _WIDGET_H_
#define _WIDGET_H_

#include <ncurses.h>

namespace Elis {
    namespace Widgets { class Base; };
};

/**
 * Interface para todos os widgets da aplicação.
 */
class Elis::Widgets::Base {
    public:
        virtual ~Base() {}

        virtual void show() = 0;
        virtual void hide() = 0;
        virtual void refresh() = 0;

        /**
         * Inicializa e configura ncurses.
         */
        static void initialize()
        {
            initscr();
            raw();
            cbreak();
            noecho();
        }

        /*
         * Finaliza o modo curses.
         */
        static void finalize()
        {
            endwin();
        }

        WINDOW* get_window()
        {
            return this->window;
        }

    protected:
        WINDOW* window;
        Elis::Widgets::Base* parent;
};

#endif
