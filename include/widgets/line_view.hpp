#ifndef _LINE_VIEW_H_
#define _LINE_VIEW_H_

#include "line.hpp"
#include "widgets/base.hpp"

namespace Elis {
    namespace Widgets { class LineView; };
};

/**
 * Exibe o conteúdo de uma linha na tela.
 *
 * Elis::LineView utiliza funções ncurses para exibir o conteúdo de uma Elis::Line
 * na tela.
 */
class Elis::Widgets::LineView : public Elis::Widgets::Base {
    public:
        LineView();
        LineView(WINDOW* window, int i, Elis::Line* l);
        ~LineView();

        void show();
        void hide();
        void refresh();

    private:
        int index;
        Elis::Line* line;

        const char * const get_format();
};

#endif
