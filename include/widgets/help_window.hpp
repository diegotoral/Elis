#ifndef _HELP_WINDOW_H_
#define _HELP_WINDOW_H_

#include "widgets/base.hpp"

namespace Elis {
    namespace Widgets { class HelpWindow; };
};

class Elis::Widgets::HelpWindow : public Elis::Widgets::Base {
    public:
        HelpWindow();
        ~HelpWindow();

        void show();
        void hide();
        void refresh();
};

#endif
