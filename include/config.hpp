#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <string>

namespace Elis {
    namespace Config
    {
        const char * const LINE_FORMAT = "%i> %s\n";
        const char * const CURRENT_LINE_FORMAT = "%i*> %s\n";
        const std::string DEFAULT_FILENAME = "untitled.txt";

        enum Keys
        {
            QUIT = 'q',
            ENTER = '\n',
            ESCAPE = 27,
            DELETE = 127,
            BACKSPACE = 0407,
        };
    };
};

#endif
