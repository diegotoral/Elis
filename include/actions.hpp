#ifndef _COMMANDS_H_
#define _COMMANDS_H_

namespace Elis {
    enum ACTION_TYPE {
        DO_NOTHING = -1,
        
        // Ações para comandos diverços da aplicação.
        EXIT = 0,
        SAVE = 1,
        OPEN = 2,
        INSERT_BEFORE = 3,
        INSERT_AFTER = 4,
        SET_CURRENT_LINE = 5,
        REMOVE_LINES = 6,
        SHOW_LINES = 7,
        HELP = 8,

        // Ações para mudança de estado da aplicação.
        TOGGLE_INSERT_MODE = 9,
        TOGGLE_NORMAL_MODE = 10,
    };
};

#endif
