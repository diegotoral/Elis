#ifndef _STRING_UTILS_H_
#define _STRING_UTILS_H_

#include <string>

#include "list.hpp"


namespace StringUtils {
    Elis::List<std::string>* split(std::string str, const char c);
};

#endif
