#ifndef _FILE_H_
#define _FILE_H_

#include <string>
#include <sstream>

#include "list.hpp"
#include "line.hpp"
#include "configuration.hpp"

namespace Elis { class File; };

class Elis::File {
    public:
        File(Elis::Configuration* c);
        ~File();

        /**
         * Cria um File a partir de uma configuração dada.
         *
         * Carrega o contúdo do arquivo especificado em Elis::Configuration para
         * a memória, mentando uma referência para um buffer. Ao final, retorna
         * o File carregado ou lança uma exceção em caso de erro.
         *
         * @param config objeto de configuração com o caminho para o arquivo.
         */
        static Elis::File* from_config(Elis::Configuration * config);

        /**
         * Carrega um arquivo a partir do caminho especificado.
         */
        static Elis::File* from_path(std::string path);

        /**
         * Número de linhas do arquivo.
         *
         * @return número de linhas.
         */
        int size();

        /**
         * Retorna as linhas do arquivo.
         *
         * @return Elis::List<Elis::Line>* ponteiro para as linhas do arquivo.
         */
        Elis::List<Elis::Line>* get_lines();

        /**
         * Returna a linha selecionada como atual.
         *
         * @return Elis::File* ponteiro para a linha atual.
         */
        Elis::Line* get_current_line();

        /**
         * Altera a linha atual.
         *
         * @param int número da linha.
         */
        void set_current_line(int index);

        /*
         * Remove a linha especificada.
         *
         * @param int número da linha.
         * @param int número da linha.
         */
        void remove_line(int number);

        /*
         * Remove linhas do arquivo conforme especificado.
         *
         * Remove as linhas do arquivo especificadas pelos números de linha start
         * e end. Mantém a linha end.
         *
         * @param int número da linha.
         * @param int número da linha.
         */
        void remove_lines(int start, int end);

        /**
         * Retorna o conteúdo do buffer em std::string.
         *
         * @return string representando o conteúdo do arquivo.
         */
        const std::string get_content();

        void save(std::string);

    private:
        Elis::Configuration* config;
        Elis::Line* current_line;
        Elis::List<Elis::Line>* lines;

        /**
        * Inicializa *lines* com as linhas contidas no buffer.
        */
        void load_lines(std::ifstream* stream);
};

#endif
