#ifndef _CONFIGURATION_H_
#define _CONFIGURATION_H_

#include <string>

namespace Elis { class Configuration; };

/**
 * Object com configurações da aplicação.
 *
 * Elis::Configuration encapusla a leitura e acesso às diversas configurações
 * disponíveis para o programa. Durante a inicialização do *elis*, um objeto de
 * configuração é inicializado e fica disponível para o restante da aplicação.
 */
class Elis::Configuration {
    public:
        /**
         * Cria uma configuração vazia.
         */
        Configuration();

        /**
         * Cria uma configuração setando o valor para a chave filename.
         *
         * @param f caminho para o arquivo.
         */
        Configuration(std::string f);

        /**
         * Inicializa uma configuração com os parâmetros lidos da linha de comando.
         *
         * Faz o parsing dos argumentos de linha de comando e inicializa um objeto
         * Elis::Configuration, retornando-o ao fim do processo.
         *
         * @param argc número de argumentos em argv.
         * @param argv argumentos da linha de comando.
         * @return um objeto de configuração com os valores lidos.
         */
        static Configuration* parse(int argc, char const *argv[]);

        /**
         * Nome do arquivo.
         *
         * @return string contendo o nome do arquivo.
         */
        std::string get_filename();

    private:
        std::string filename;
};

#endif
