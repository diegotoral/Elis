namespace :test do
  test_files = FileList["#{TEST_DIR}/**/*#{TEST_EXT}"]
  test_objects = test_files.ext(OBJ_EXT).sub(TEST_DIR, BUILD_DIR)
  source_files = FileList.new("#{SRC_DIR}/**/*#{SRC_EXT}") do |f|
    f.exclude('**/main.cpp')
  end
  object_files = source_files.ext(OBJ_EXT).gsub(SRC_DIR, BUILD_DIR)

  desc "Build test runner."
  task build: [:setup, TEST_TARGET]

  file TEST_TARGET => test_objects + object_files do |t|
    sh "#{CC} #{INC} #{t.prerequisites.join(' ')} -o #{TEST_TARGET}  -lncurses"
  end

  rule OBJ_EXT => [->(f){ test_path(f) }] do |r|
    mkdir_p r.name.pathmap("%d")
    sh "#{CC} #{CFLAGS} #{INC} -o #{r.name} -c #{r.prerequisites.join(' ')}"
  end
end
