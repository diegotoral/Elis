namespace :source do
  source_files = FileList["#{SRC_DIR}/**/*#{SRC_EXT}"]
  object_files = source_files.ext(OBJ_EXT).gsub(SRC_DIR, BUILD_DIR)

  desc "Build application."
  task build: [:setup, TARGET]

  file TARGET => object_files do |t|
    sh "#{CC} #{t.prerequisites.join(' ')} -o #{TARGET} -lncurses"
  end

  rule OBJ_EXT => [->(f){ src_path(f) }] do |r|
    mkdir_p r.name.pathmap("%d")
    sh "#{CC} #{CFLAGS} -o #{r.name} -c #{r.prerequisites.join(' ')} #{INC}"
  end
end
