def src_path(path)
  path.gsub(BUILD_DIR, SRC_DIR).gsub(OBJ_EXT, SRC_EXT)
end

def test_path(path)
  path.gsub(BUILD_DIR, TEST_DIR).gsub(OBJ_EXT, SRC_EXT)
end
